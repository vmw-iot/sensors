# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VCC | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |

## Install Dependencies
`sudo apt install python-smbus i2c-tools`

## Install Driver
```bash
cd ~
mkdir -p drivers/bmp180
cd drivers/bmp180

curl -ko I2C.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_GPIO/master/Adafruit_GPIO/I2C.py
curl -ko Platform.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_GPIO/master/Adafruit_GPIO/Platform.py
curl -ko BMP085.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_BMP/master/Adafruit_BMP/BMP085.py
curl -ko smbus.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_PureIO/master/Adafruit_PureIO/smbus.py

curl -ko test.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_BMP/master/examples/simpletest.py
```
