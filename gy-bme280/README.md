# Info

[Electroschematics Guide](https://www.electroschematics.com/bmp280-diy-project-primer/)
[CircuitPython BMP280](https://circuitpython.readthedocs.io/projects/bmp280/en/latest/)
[API Reference](https://circuitpython.readthedocs.io/projects/bmp280/en/latest/api.html)
[Adafruit Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_BMP280)
[Pimoroni Driver Git](https://github.com/pimoroni/bme280-python)
[Adafruit Learning](https://learn.adafruit.com/adafruit-bmp280-barometric-pressure-plus-temperature-sensor-breakout/overview)

I2C Address: 0x76
Chip ID: 0x58

# Preparation

## Wiring I2C
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |

## Wiring SPI
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCLK        |
| SDA | MOSI        |
| CSB | GPIO 5 (any)|
| SDO | MISO        |


SPI Logic pins:
SCK - This is the SPI Clock pin, its an input to the chip
SDO - this is the Serial Data Out / Master In Slave Out pin, for data sent from the BMP280 to your processor
SDI - this is the Serial Data In / Master Out Slave In pin, for data sent from your processor to the BMP280
CS - this is the Chip Select pin, drop it low to start an SPI transaction. Its an input to the chip

If you want to connect multiple BMP280's to one microcontroller, have them share the SDI, SDO and SCK pins. Then assign each one a unique CS pin.

I2C Logic pins:
SCK - this is also the I2C clock pin, connect to your microcontrollers I2C clock line.
SDI - this is also the I2C data pin, connect to your microcontrollers I2C data line.

## Driver
`sudo pip3 install adafruit-circuitpython-bmp280`

# Test
`python3 test.py`
