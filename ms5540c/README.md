# Info

I2C Address: ??

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VCC | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCLK | SCL (pin 5) |

## Driver
``

# Test
`python3 test.py`
