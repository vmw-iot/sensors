# Info

[CircuitPython ccs811](https://circuitpython.readthedocs.io/projects/ccs811/en/latest/)
[API Reference](https://circuitpython.readthedocs.io/projects/ccs811/en/latest/api.html)
[Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_CCS811)
[Arduino Library Git](https://github.com/maarten-pennings/CCS811)
[Adafruit Learning](https://learn.adafruit.com/adafruit-ccs811-air-quality-sensor)
[Sparkfun Learning](https://learn.sparkfun.com/tutorials/ccs811-air-quality-breakout-hookup-guide)
[Revspace Guide](https://revspace.nl/CJMCU-811)
[AMS Product Page](https://ams.com/ccs811)
[Firmware Upgrade](https://github.com/maarten-pennings/CCS811/tree/master/examples/ccs811flash)

I2C Address: 0x5A (if ADDR tied to GND) or 0x5B (default)

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VCC | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |
| WAK | GND         |
| INT | |
| RST | |
| ADD | |

SCL - this is the I2C clock pin, connect to your microcontrollers I2C clock line. There is a 10K pullup on this pin and it is level shifted so you can use 3 - 5VDC.
SDA - this is the I2C data pin, connect to your microcontrollers I2C data line. There is a 10K pullup on this pin and it is level shifted so you can use 3 - 5VDC.
INT - this is the interrupt-output pin. It is 3V logic and you can use it to detect when a new reading is ready or when a reading gets too high or too low.
WAKE - this is the wakeup pin for the sensor. It needs to be pulled to ground in order to communicate with the sensor. This pin is level shifted so you can use 3-5VDC logic.
RST - this is the reset pin. When it is pulled to ground the sensor resets itself. This pin is level shifted so you can use 3-5VDC logic.

## Driver
`sudo pip3 install adafruit-circuitpython-ccs811`

## Slow down I2C Clock
  - edit `/boot/config.txt`
  - uncomment lines:
    ```
    dtparam=i2c_arm=on
    dtparam=i2s=on
    dtparam=spi=on
    ```
  - add after:
    ```
    # clock stretch by slow down to 10KHz
    dtparam=i2c_arm_baudrate=10000
    ```
  - reboot
  - if still got bad data, slow down to 5KHz or even 1KHz, reboot after each change

# Test
`sudo i2cdetect -y 1`
`python3 test.py`
