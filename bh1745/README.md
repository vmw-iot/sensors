# Info

[ControlEverything Driver Git](https://github.com/ControlEverythingCommunity/BH1745NUC)
[Adafruit Learning?](https://learn.adafruit.com/tsl2561)
[Arduino Learning](http://arduinolearning.com/code/arduino-and-bh1745nuc-luminance-and-colour-sensor-example.php)

I2C Address: 0x39?
It is possible to select 2 type of I²C bus slave address (ADDR ='L': "0111000", ADDR ='H': "0111001")
The ADDR pin can be used if you have an i2c address conflict, to change the address. Connect it to ground to set the address to 0x29, connect it to 3.3V (vcc) to se t the address to 0x49 or leave it floating (unconnected) to use address 0x39.

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |
| INT | |
| ADD |

The INT pin is an ouput from the sensor used when you have the sensor configured to signal when the light level has changed. We don't have that code written in this tutorial so you don't have to use it. If you do end up using it, use a 10K-100K pullup from INT to 3.3V (vcc)

## Driver
`sudo pip3 install adafruit-circuitpython-tsl2561?`

# Test
`python3 test.py`
