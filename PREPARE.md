# General Preparation

[Adafruit Guide](https://learn.adafruit.com/circuitpython-on-raspberrypi-linux)

1. Prepare Raspberry Pi with Raspbian Lite
2. Check SSH access to the RPi
3. Update RPi: `sudo apt update && sudo apt upgrade`
4. Install required packages:
    ```bash
    sudo apt install -y python3 python3-dev python3-distutils python-smbus i2c-tools
    curl https://bootstrap.pypa.io/get-pip.py -ko get-pip.py
    sudo python3 get-pip.py
    sudo reboot
    ```
5. Check I2C & SPI:
    ```bash
    ls /dev/spi* /dev/i2c*
    i2cdetect -y 1
    ```
6. (Optional) Enable second SPI port by adding `dtoverlay=spi1-3cs` to bottom of the /boot/config.txt
7. Install Python Libraries:
    ```bash
    sudo pip3 install RPI.GPIO
    sudo pip3 install adafruit-blinka
    ```
8. Test with code:
    ```python
    import board
    import digitalio
    import busio

    print("Hello blinka!")

    # Try to great a Digital input
    pin = digitalio.DigitalInOut(board.D4)
    print("Digital IO ok!")

    # Try to create an I2C device
    i2c = busio.I2C(board.SCL, board.SDA)
    print("I2C ok!")

    # Try to create an SPI device
    spi = busio.SPI(board.SCLK, board.MOSI, board.MISO)
    print("SPI ok!")

    print("done!")
    ```

# Sensor Drivers

- tcs34725 - [Color Sensor](tcs34725/README.md)
- ms5540c - [Barometer Module](ms5540c/README.md)
- sht3x - [Humidity & Temperature](sht3x/README.md)
- gy-bme280 - [Pressure/Humidity/Temperature](gy-bme280/README.md)
- bme680 - [Gas/Pressure/Temp/Hum](bme680/README.md)
- bh1745 - [Ambient Light and Color](bh1745/README.md)
- ccs811 - [GAS sensor](ccs811/README.md)
- ds3231 - [Real Time Clock](ds3231/README.md)

# Maintaince

- Check for updates: `sudo apt update && sudo apt upgrade`
- Update pip3: `sudo pip3 install --upgrade setuptools`
- Adafruit updates: `sudo pip3 install --upgrade adafruit_blinka`
