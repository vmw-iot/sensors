# Simple demo of the TCS34725 color sensor.
# Will detect the color from the sensor and print it out every second.
import time

import board
import busio

import adafruit_tcs34725


# Initialize I2C bus and sensor.
i2c = busio.I2C(board.SCL, board.SDA)
sensor = adafruit_tcs34725.TCS34725(i2c)

# sensor.integration_time = 200  # the integration time of the sensor in milliseconds.  Must be a value between 2.4 and 614.4
# sensor.gain = 60  # the gain of the sensor, must be a value of 1, 4, 16, 60

# Main loop reading color and printing it every second.
while True:
    # Read the color, temperature and lux of the sensor
    print('Color: ({0}, {1}, {2}), Temperature: {3}K Lux: {4}'.format(*sensor.color_rgb_bytes, sensor.color_temperature, sensor.lux))
    # Delay for a second and repeat.
    time.sleep(1.0)
