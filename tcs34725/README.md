# Info

[CircuitPython tcs34725](https://circuitpython.readthedocs.io/projects/tcs34725/en/latest/)
[API Notes](https://circuitpython.readthedocs.io/projects/tcs34725/en/latest/api.html)
[Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_TCS34725)
[Adafruit Learning](https://learn.adafruit.com/adafruit-color-sensors)

I2C Address: 0x29

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |

## Driver
`sudo pip3 install adafruit-circuitpython-tcs34725`

# Test
`python3 test.py`
