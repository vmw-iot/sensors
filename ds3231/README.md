# Info

[CircuitPython DS3231](https://circuitpython.readthedocs.io/projects/ds3231)
[API Reference](https://circuitpython.readthedocs.io/projects/ds3231/en/latest/api.html)
[Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_DS3231)
[Adafruit Learning](https://learn.adafruit.com/adafruit-ds3231-precision-rtc-breakout/)

I2C Address: 0x

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |

BAT - this is the same connection as the positive pad of the battery. You can use this if you want to power something else from the coin cell, or provide battery backup from a different separate batery. VBat can be between 2.3V and 5.5V and the DS3231 will switch over when main Vin power is lost
32K - 32KHz oscillator output. Open drain, you need to attach a pullup to read this signal from a microcontroller pin
SQW - optional square wave or interrupt output. Open drain, you need to attach a pullup to read this signal from a microcontroller pin
RST - This one is a little different than most RST pins, rather than being just an input, it is designed to be used to reset an external device or indicate when main power is lost. Open drain, but has an internal 50K pullup. The pullup keeps this pin voltage high as long as Vin is present. When Vin drops and the chip switches to battery backup, the pin goes low.

## Driver
`sudo pip3 install adafruit-circuitpython-ds3231`

# Test
`python3 test.py`
