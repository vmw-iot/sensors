# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VCC | 5V (Pin 2) |
| OUT | GPIO23 (Pin 16) |
| GND | GND (pin 6) |

## Install Dependencies
//`sudo apt install python-smbus i2c-tools`

## Install Driver
```bash
cd ~
mkdir -p drivers/pir
cd drivers/pir

curl -ko RPi.GPIO-0.7.0.tar.gz https://netix.dl.sourceforge.net/project/raspberry-gpio-python/RPi.GPIO-0.7.0.tar.gz

curl -ko test.py https://raw.githubusercontent.com/adafruit/Adafruit_Python_BMP/master/examples/simpletest.py
```
