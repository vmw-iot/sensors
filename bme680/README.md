# Info

[CircuitPython bme680](https://circuitpython.readthedocs.io/projects/bme680/en/latest/)
[API Reference](https://circuitpython.readthedocs.io/projects/bme680/en/latest/api.html)
[Adafruit Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_BME680)
[Pimoroni Driver Git](https://github.com/pimoroni/bme680-python)
[Bosch Driver Git](https://github.com/BoschSensortec/BME680_driver)
[Adafruit Learning](https://learn.adafruit.com/adafruit-bme680-humidity-temperature-barometic-pressure-voc-gas/)
[Pimoroni Learning](https://learn.pimoroni.com/tutorial/sandyj/getting-started-with-bme680-breakout)

I2C Address: 0x77 or 0x76 (add jumper between SDO and GND)

# Preparation

## Wiring I2C
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCL (pin 5) |
| SDA | SDA (Pin 3) |

## Wiring SPI
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL | SCLK        |
| SDA | MOSI        |
| CS  | GPIO 5 (any)|
| SDO | MISO        |

## Additional Info

SPI Logic pins:
All pins going into the breakout have level shifting circuitry to make them 3-5V logic level safe. Use whatever logic level is on Vin!
SCK - This is the SPI Clock pin, its an input to the chip
SDO - this is the Serial Data Out / Master In Slave Out pin, for data sent from the BME680 to your processor
SDI - this is the Serial Data In / Master Out Slave In pin, for data sent from your processor to the BME680
CS - this is the Chip Select pin, drop it low to start an SPI transaction. Its an input to the chip
If you want to connect multiple BME680's to one microcontroller, have them share the SDI, SDO and SCK pins. Then assign each one a unique CS pin.

I2C Logic pins:
SCK - this is also the I2C clock pin, connect to your microcontrollers I2C clock line.
SDI - this is also the I2C data pin, connect to your microcontrollers I2C data line.

Leave the other pins disconnected

## Driver
`sudo pip3 install adafruit-circuitpython-bme680`

# Test
`python3 test.py`
