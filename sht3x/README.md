# Info

[CircuitPython SHT31-D](https://circuitpython.readthedocs.io/projects/sht31d/en/latest/)
[API Notes](https://circuitpython.readthedocs.io/projects/sht31d/en/latest/)
[Driver Git](https://github.com/adafruit/Adafruit_CircuitPython_SHT31D)
[Adafruit Learning](https://learn.adafruit.com/adafruit-sht31-d-temperature-and-humidity-sensor-breakout)

I2C Address: 0x44 or 0x45

# Preparation

## Wiring
| Sensor | Raspberry Pi |
|----------|----------|
| VIN | 3V3 (Pin 1) |
| GND | GND (Pin 6) |
| SCL/T | SCL (pin 5) |
| SDA/RH | SDA (Pin 3) |

## Driver
`sudo pip3 install adafruit-circuitpython-sht31d`

# Test
`python3 test.py`
